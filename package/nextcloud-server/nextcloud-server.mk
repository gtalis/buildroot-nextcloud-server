################################################################################
#
# nextcloud-server
#
################################################################################

NEXTCLOUD_SERVER_VERSION = 21.0.7
NEXTCLOUD_SERVER_SOURCE = nextcloud-$(NEXTCLOUD_SERVER_VERSION).zip
NEXTCLOUD_SERVER_SITE = https://download.nextcloud.com/server/releases
NEXTCLOUD_SERVER_LICENSE = AGPL-3.0
NEXTCLOUD_SERVER_LICENSE_FILES = COPYING
NEXTCLOUD_SERVER_DEPENDENCIES = apache mysql php

define NEXTCLOUD_SERVER_EXTRACT_CMDS
# 	Nothing to do. nextcloud files will directly be installed in target dir
endef

define NEXTCLOUD_SERVER_INSTALL_TARGET_CMDS
	$(UNZIP) -d $(TARGET_DIR)/var/www $(NEXTCLOUD_SERVER_DL_DIR)/$(NEXTCLOUD_SERVER_SOURCE)
endef

define NEXTCLOUD_SERVER_PERMISSIONS
	/var/www/nextcloud r 755 33 33 - - - - -
endef

define NEXTCLOUD_SERVER_INSTALL_HTTPD_CONF
	$(INSTALL) -D -m 0644 $(NEXTCLOUD_SERVER_PKGDIR)/httpd.conf \
		$(TARGET_DIR)/etc/apache2/httpd.conf
endef

NEXTCLOUD_SERVER_POST_INSTALL_TARGET_HOOKS += NEXTCLOUD_SERVER_INSTALL_HTTPD_CONF

$(eval $(generic-package))
