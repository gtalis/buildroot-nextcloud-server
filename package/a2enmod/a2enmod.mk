################################################################################
#
# a2enmod
#
################################################################################

A2ENMOD_VERSION = 1.2
A2ENMOD_SITE = $(call github,dracorp,a2enmod,$(A2ENMOD_VERSION))
A2ENMOD_LICENSE = Apache-2.0
A2ENMOD_LICENSE_FILES = a2enmod

define A2ENMOD_INSTALL_TARGET_CMDS
	$(TARGET_MAKE_ENV) $(MAKE) -C $(@D) DESTDIR=$(TARGET_DIR) install
endef

$(eval $(generic-package))
